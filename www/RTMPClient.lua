		
AMFTypes = {CHUNCKSIZE=0x01,AMF_WITH_HANDLER=0x14,AUDIO=0x08,VIDEO=0x09,AMF=0x0F,SERVER_BANDWITH=0x05,UNKNOWN=0x00}
--io.output(cumulus:absolutePath("").."log.txt")

function hexDump(buf,len)
      for i=1,math.ceil(len/16) * 16 do
         if (i-1) % 16 == 0 then io.write(string.format('%08X  ', i-1)) end
         io.write( i > len and '   ' or string.format('%02X ', buf:byte(i)) )
         if i %  8 == 0 then io.write(' ') end
         if i % 16 == 0 then io.write( buf:sub(i-16+1, i):gsub('%c','.'), '\n' ) end
      end
end

function encode16(value)
	local temp = math.floor(value/256)
	return string.char(temp)..string.char(value-temp*256)
end

function encode24(value)
	local temp = math.floor(value/256)
	return encode16(temp)..string.char(value-temp*256)
end

function encode32(value)
	local temp = math.floor(value/256)
	return encode24(temp)..string.char(value-temp*256)
end

function normalizeType(type)
	if type==0x11 then
		return AMFTypes.AMF_WITH_HANDLER
	end
	if type ~= AMFTypes.AMF_WITH_HANDLER and type ~= AMFTypes.SERVER_BANDWITH and type ~= AMFTypes.CHUNCKSIZE and type ~= AMFTypes.AUDIO and type ~= AMFTypes.VIDEO and type ~= AMFTypes.AMF then
		return AMFTypes.UNKNOWN
	end
	return type
end


-- BEHAVIOR
function createRTMPClient(path,swfUrl,pageUrl,flashVersion)
	-- rtmp Client
	local client = {socket=cumulus:createTCPClient(),streams={[1]=false,[2]=false},channels={},requests={},chunksizeSend=128,chunksizeRecv=128,path=path,swfUrl=swfUrl,pageUrl=pageUrl,flashVersion=flashVersion}
	client.socket.client = client
	
	function client.socket:onReception(packet)
		return self.client:onReception(packet)
	end
	function client.socket:onDisconnection()
		self.client.error = self.error
		if self.client.onDisconnection then self.client:onDisconnection() end
	end

	function client:connect(host,port)
		-- connection
		self.error = self.socket:connect(host,port)
		if self.error then return self.error end
		
		-- handshake
		local data = string.char(3)..string.char(0)..string.char(0)..string.char(0)..string.char(0)..string.char(0)..string.char(0)..string.char(0)..string.char(0)
		for i=1,1528,1 do
			data = data..string.char(math.random(0,255))
		end
		self.socket:send(data)
	end
	
	function client:connected()
		return self.socket.connected
	end
	
	function client:disconnect()
		self.socket:disconnect()
		self.streams,self.requests,self.channels,self.chunksizeSend,self.chunksizeRecv = {[1]=false,[2]=false},{},{},128,128
	end
	
	function client:onReception(packet)
		if not self.handshake then
			-- handshake
			if #packet<3073 then return 0 end
			self.socket:send(string.sub(packet,1538,3073))
			self.handshake = true
			-- send first packet connection
			local stream = self:stream()
			client.chunksizeSend=4096
			stream:send(0,0x01,encode32(client.chunksizeSend))
			stream:request({name="connect"},0,
				cumulus:toAMF0({app=string.sub(self.path,2),flashVer=self.flashVersion,swfUrl=self.swfUrl,tcUrl="rtmp://"..self.socket.peerAddress..self.path,
				fpad=false,capabilities=239,audioCodecs=3575,videoCodecs=252,videoFunction=1,pageUrl=self.pageUrl,objectEncoding=3})
			)
			return 3073
		else
			if self:receive(packet) then return #packet end
		end
		return 0
	end

	function client:publish(name)
		local publication = self:newStream()
		publication.name = name
		publication.channel=0 -- to get a 0x11 a "publish" request of type 0x11
		self:stream():request({name="createStream",publication=publication,type=0x11},0)
		function publication:pushVideoPacket(time,packet)
			self:send(time,0x09,packet)
		end
		function publication:pushAudioPacket(time,packet)
			self:send(time,0x08,packet)
		end
		return publication
	end
	
	function client:play(name)
		local listener = self:newStream()
		listener.name = name
		listener.channel=0 -- to get a 0x11 a "play" request of type 0x11
		self:stream():request({name="createStream",listener=listener,type=0x11},0)
		return listener
	end
	
	function client:newStream()
		return self:stream(#self.streams+1)
	end
	
	function client:stream(id)
		if not id then id=3 end
		local stream = self.streams[id]
		if not stream then
			stream = {id=id,client=self}
			function stream:unpackResponse(name,request,null,...)
				return name,request,null,arg
			end
			function stream:flush()
				if self.onFlush then self:onFlush() end
				if self.channelRecv and self.channelRecv~=self then self.channelRecv:flush() end
			end
			function stream:receive(time,type,packet)
				if type == AMFTypes.AMF_WITH_HANDLER then
					local name,request,null,values = stream:unpackResponse(cumulus:fromAMF(packet))
					-- NOTE("response : "..name)
					local question = self.client.requests[request]
					self.client.requests[request] = nil
					if name == "_result" then
						if question then
							-- NOTE("question.name : "..question.name)
							if question.name == "createStream" then
								if question.publication then
									-- New publisher
									-- NOTE("question.publication : "..question.publication.name)
									question.publication.channel = values[1]
									self.client.channels[question.publication.channel] = question.publication
									question.publication:request({name="publish",publication=question.publication},0,cumulus:toAMF0(question.publication.name))
								elseif question.listener then
									-- New subscriber
									-- NOTE("question.listener : "..question.listener.name)
									question.listener.channel = values[1]
									self.client.channels[question.listener.channel] = question.listener
									question.listener:request("play",0,cumulus:toAMF0(question.listener.name))
								end
							end
						end
					elseif name == "_error" then
						ERROR("_error")
					elseif name == "onStatus" then
						if question then
							if question.name == "publish" and question.publication and question.publication.onBadName then
								question.publication:onBadName()
							end
						end
					end
				elseif type == AMFTypes.CHUNCKSIZE then
					self.client.chunksizeRecv = packet:byte(1)*16777216+packet:byte(2)*65536+packet:byte(3)*256+packet:byte(4)
				elseif type == AMFTypes.AUDIO then
					local recv = self.channelRecv
					if recv and recv.onAudioPacket then
						if time>0 and not recv.deltaTime then recv.deltaTime = time end
						recv:onAudioPacket(recv.deltaTime and (time-recv.deltaTime) or 0,packet)
					end
				elseif type == AMFTypes.VIDEO then
					local recv = self.channelRecv
					if recv and recv.onVideoPacket then
						if time>0 and not recv.deltaTime then recv.deltaTime = time end
						recv:onVideoPacket(recv.deltaTime and (time-recv.deltaTime) or 0,packet)
					end
				end
			end
			function stream:close()
				self:request("closeStream",0)
				self.client.streams[self.id] = nil
				if self.channel>0 then self.client.channels[self.channel]=nil end
				self.closed=true
			end
			function stream:request(question,time,content)
				local request = 0
				local name = ""
				local type = self.channel and 0x11 or AMFTypes.AMF_WITH_HANDLER
				if _G.type(question) == "table" then
					request = #self.client.requests+1
					self.client.requests[request] = question
					name = question.name
					if question.type then type = question.type end
				else
					name = question
				end
				if not content then content="" end
				if name =="connect" then
					self:send(time,type,cumulus:toAMF0(name,request)..content)
				else
					self:send(time,type,cumulus:toAMF0(name,request,nil)..content)
				end
			end
			function stream:send(time,type,content)
				-- header
				local size = type==0x11 and (#content+1) or #content
				local headerSize,headerFlag = 12,0
				if self.channelSend and self.channelSend == self.channel then
					headerSize,headerFlag = 8,1
					if self.typeSend == type and self.sizeSend == size then
						headerSize,headerFlag = 4,2
						if self.timeSend == time then headerSize,headerFlag = 1,3 end
					end
				end
				
				local packet = string.char(headerFlag*64+self.id)
				if self.timeSend and time<self.timeSend then time=self.timeSend end
				if headerSize>1 then
					if time<0xFFFFFF then
						packet = packet..encode24(headerSize==12 and time or (time-self.timeSend))
					else
						packet = packet..string.char(0xFF)..string.char(0xFF)..string.char(0xFF)
					end
					if headerSize>4 then
						packet = packet..encode24(size)
						packet = packet..string.char(type)
						if headerSize>8 then
							packet = packet..string.char(self.channel and self.channel or 0)..string.char(0)..string.char(0)..string.char(0)
						end
					end
					if time>=0xFFFFFF then
						packet = packet..encode24(headerSize==12 and time or (time-self.timeSend))
						headerSize = headerSize+4
					end
				end
				if type==0x11 then packet = packet..string.char(0) end
				self.channelSend,self.typeSend,self.sizeSend,self.timeSend = self.channel,type,size,time
				
				-- content
				packet = packet..content
				
				-- send
				repeat
					local fragment = string.sub(packet,1,self.client.chunksizeSend+headerSize)
					packet = string.sub(packet,self.client.chunksizeSend+headerSize+1)
					if headerSize>0 then headerSize = 0	else fragment = string.char(0xC0+self.id)..fragment end
					self.client.socket:send(fragment)
				--	if type~=0x09 then hexDump(fragment,#fragment) end
				until #packet==0
			end
			self.streams[id] = stream
		end
		return stream
	end
	
	function client:receive(packet)
		local consumed,stream = 0,nil
		repeat
		--	hexDump(packet,#packet)
			local headerSize = math.floor(packet:byte(1)/64)
			local newStream = self:stream(packet:byte(1)-(headerSize*64))
			if newStream ~= stream then	if stream then stream:flush() end stream = newStream end
	
			if headerSize==0 then
				headerSize=12
			elseif headerSize == 1 then
				headerSize = 8
			elseif headerSize == 2 then
				headerSize = 4
			else
				headerSize = 1
			end
			if #packet < headerSize then return consumed end
			
			local message = string.sub(packet,headerSize+1,headerSize+self.chunksizeRecv)
			
			if headerSize==1 then
				if stream.messageRecv then
					stream.messageRecv = stream.messageRecv..message
				else
					stream.messageRecv = message
				end
			elseif headerSize>=4 then
				stream.messageRecv = message
				local time = packet:byte(2)*65536+packet:byte(3)*256+packet:byte(4)
				if time==0xFFFFFF then
					time = packet:byte(headerSize+1)*16777216+packet:byte(headerSize+2)*65536+packet:byte(headerSize+3)*256+packet:byte(headerSize+4)
				end
				if headerSize>=8 then
					stream.sizeRecv = packet:byte(5)*65536+packet:byte(6)*256+packet:byte(7)
					stream.typeRecv = normalizeType(packet:byte(8))
					if headerSize>=12 then
						local channelRecv = packet:byte(12)*16777216+packet:byte(11)*65536+packet:byte(10)*256+packet:byte(9)
						if channelRecv>0 then stream.channelRecv=self.channels[channelRecv] end
						stream.timeRecv = time -- time
					else
						stream.timeRecv = stream.timeRecv+time -- delta
					end
				else
					stream.timeRecv = stream.timeRecv+time -- delta
				end
		--		NOTE("headerSize/timeRecv : "..headerSize.."/"..stream.timeRecv)
			end
			
			if stream.typeRecv == 0x11 then headerSize = headerSize+1 end
			
			if #stream.messageRecv > stream.sizeRecv then
				message = string.sub(message,#stream.messageRecv-stream.sizeRecv+1)
				stream.messageRecv = string.sub(stream.messageRecv,1,stream.sizeRecv)
			end

			if #stream.messageRecv == stream.sizeRecv then
				stream:receive(stream.timeRecv,stream.typeRecv,stream.messageRecv)
				stream.messageRecv=nil
			end
			
			packet = string.sub(packet,headerSize+#message+1)
			consumed = consumed + headerSize + #message
		until #packet==0
		if stream then stream:flush() end
		
		return consumed
	end
	
	return client
end