CRTMPSERVER_HOST = "localhost"
CRTMPSERVER_PORT = 1935

dofile(cumulus:absolutePath("").."RTMPClient.lua")

-- RTMFP events!
function onConnection(client,response,...)
	NOTE("RTMP client connection")
	client.rtmp = createRTMPClient(client.path,client.swfUrl,client.pageUrl,client.flashVersion)
	client.rtmp.rtmfp = client
	function client.rtmp:onDisconnection()
		if self.error then
			if not self.rtmfp.disconnection then self.rtmfp.writer:writeStatusResponse("Connect.Rejected",error) end
			ERROR(self.error)
		elseif not self.rtmfp.disconnection then 
			self.rtmfp.writer:writeStatusResponse("Connect.Rejected","Connection closed by the server (Maybe '"..client.path.."' application doesn't exist)");
			self.rtmfp:close()
		end
		NOTE("RTMP client disconnection")
	end
	local err = client.rtmp:connect(CRTMPSERVER_HOST,CRTMPSERVER_PORT)
	if err then error(err) end -- it closes the RTMFP connection
end

function onPublish(client,publication)
	if not client.rtmp:connected() then return end
	publication.rtmp = client.rtmp:publish(publication.name)
	publication.rtmp.rtmfp = publication
	function publication.rtmp:onBadName()
		if self.closed then return end
		publication:close("Publish.BadName",publication.name.." is already published")
	end
end

function onUnpublish(client,publication)
	if not client.rtmp:connected() then return end
	publication.rtmp:close()
end

function onVideoPacket(client,publication,time,packet)
	if not client.rtmp:connected() then return end
	publication.rtmp:pushVideoPacket(time,packet)
end

function onAudioPacket(client,publication,time,packet)
	if not client.rtmp:connected() then return end
	publication.rtmp:pushAudioPacket(time,packet)
end

function onSubscribe(client,listener)
	if not client.rtmp:connected() then return end
	listener.rtmp = client.rtmp:play(listener.publication.name)
	listener.rtmp.rtmfp = listener
	if listener.publication.listeners.count==0 then
		function listener.rtmp:onAudioPacket(time,packet)
			if self.closed then return end
			local publication = self.rtmfp.publication
			if publication.publisherId==0 then cumulus:publish(publication.name) end
			publication:pushAudioPacket(time,packet)
		end
		function listener.rtmp:onVideoPacket(time,packet)
			if self.closed then return end
			local publication = self.rtmfp.publication
			if publication.publisherId==0 then cumulus:publish(publication.name) end
			publication:pushVideoPacket(time,packet)
		end
		function listener.rtmp:onFlush()
			if not self.closed then self.rtmfp.publication:flush() end
		end
	end
end

function onUnsubscribe(client,listener)
	if not client.rtmp:connected() then return end
	listener.rtmp:close()
	if listener.publication.listeners.count==1 then listener.publication:close() end
end

function onDisconnection(client)
	if not client.rtmp:connected() then return end
	client.disconnection=true
	client.rtmp:disconnect()
end